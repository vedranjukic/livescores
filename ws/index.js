var app = require('express')();
var http = require('http').Server(app);
var redis = require('redis');
var io = require('socket.io')(http);

var sub = redis.createClient(process.env.REDIS_HOST);

sub.subscribe('admin');

sub.on('message', (channel, message) => {
  console.log(channel, message);
  var m = JSON.parse(message);
  io.emit(m.event, m.data);

});

io.on('connection', function(socket) {
  console.log('a user connected');
});

//  TODO: add config from ENV
http.listen(process.env.PORT || 9000, function() {
  console.log('listening on *:' + process.env.PORT || 9000);
});
