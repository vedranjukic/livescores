require 'bundler/setup'
Bundler.require(:default)

client = Mysql2::Client.new(:host => ENV['DB_HOST'], :username => ENV['DB_USERNAME'], :password => ENV['DB_PASSWORD'], :database => ENV['DB_DATABASE'])

configure do
  enable :cross_origin
end

before do
  if request.request_method == 'OPTIONS'
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Methods"] = "POST"

    halt 200
  end
end

# list all
get '/fixtures' do
  results = client.query("SELECT id, title, (SELECT score FROM scores WHERE fixture_id = fixtures.id ORDER BY scores.id DESC LIMIT 1) score FROM fixtures ORDER BY id");
  results.to_a.to_json
end

post '/fixtures' do
  results = client.query("SELECT id, title, (SELECT score FROM scores WHERE fixture_id = fixtures.id ORDER BY scores.id DESC LIMIT 1) score FROM fixtures ORDER BY id");
  results.to_a.to_json
end