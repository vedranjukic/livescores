import React from 'react';

export default class Fixtures extends React.Component {
  
  constructor(props) {

    super(props);

  }

  renderFixtures() {

    if (!Array.isArray(this.props.fixtures))
      return;

    var fixtures = [];

    this.props.fixtures.forEach( fixture => {


      fixtures.push( <div key={fixture.id}><span className="title">{fixture.title}</span><span className="score">{fixture.score}</span></div> );

    });

    return fixtures;

  }

  render() {

    return (

      <div className="fixtures">
        {this.renderFixtures()}
      </div>

    );

  }

}