import io from 'socket.io-client';
import axios from 'axios';

import config from '../appconf';
import Fixtures from './fixtures'

import React from 'react';
import ReactDOM from 'react-dom';

class App {

  constructor() {

    this.config = config;
    this.socket = io(this.config.wsUrl);

    this.socket.on('fixture_add', fixture => {
      
      var data = this.data;
      data.push(fixture);
      this.data = data;

    });

    this.socket.on('fixture_update', fixture => {
      
      var data = this.data;

      for (var i=0; i<data.length; i++) {
        if (data[i].id == fixture.id)
          data[i] = fixture;
      }

      this.data = data;

    });

    this.socket.on('fixture_delete', fixture_id => {
      
      var data = this.data;

      for (var i=0; i<data.length; i++) {
        if (data[i].id == fixture_id)
          data.splice(i);
      }

      this.data = data;

    });

    this.socket.on('score_change', score => {
      
      var data = this.data;

      for (var i=0; i<data.length; i++) {
        if (data[i].id == score.fixture_id)
          data[i].score = score.score;
      }

      this.data = data;

    });

    this.divEl = document.createElement("div");
    document.body.appendChild(this.divEl);

    this.refresh();

  }

  get data() {
    return this._data;
  }
  set data(data) {

    this._data = data;

    ReactDOM.render(
       <Fixtures fixtures={data} />,
       this.divEl
    );

  }

  refresh() {

    this.request('/fixtures', {}, (err, data) => {

      this.data = data;

    });

  }

  request(method, data, cb) {

    axios.post(this.config.apiUrl + method, data)
    .then(xhr => {
      if (cb)
        cb(null, xhr.data);
     })
     .catch(e => {
       if (cb)
        cb(e);
     });

  }
  
}

window.app = new App();