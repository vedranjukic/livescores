var gulp = require('gulp');
var replace = require('gulp-replace');
var rename = require("gulp-rename");
 
gulp.task('config', function(){
  gulp.src(['appconf.template.js'])
    .pipe(replace(/\{API_URL\}/g, process.env.API_URL))
    .pipe(replace(/\{WS_URL\}/g, process.env.WS_URL))
    .pipe(rename('appconf.js'))
    .pipe(gulp.dest('./'));
});