/* */ 
(function(process) {
  'use strict';
  var utils = require('../utils');
  var buildURL = require('../helpers/buildURL');
  var parseHeaders = require('../helpers/parseHeaders');
  var transformData = require('../helpers/transformData');
  var isURLSameOrigin = require('../helpers/isURLSameOrigin');
  var btoa = (typeof window !== 'undefined' && window.btoa) || require('../helpers/btoa');
  var settle = require('../helpers/settle');
  module.exports = function xhrAdapter(resolve, reject, config) {
    var requestData = config.data;
    var requestHeaders = config.headers;
    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type'];
    }
    var request = new XMLHttpRequest();
    var loadEvent = 'onreadystatechange';
    var xDomain = false;
    if (process.env.NODE_ENV !== 'test' && typeof window !== 'undefined' && window.XDomainRequest && !('withCredentials' in request) && !isURLSameOrigin(config.url)) {
      request = new window.XDomainRequest();
      loadEvent = 'onload';
      xDomain = true;
    }
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }
    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);
    request.timeout = config.timeout;
    request.onprogress = function handleProgress() {};
    request.ontimeout = function handleTimeout() {};
    request[loadEvent] = function handleLoad() {
      if (!request || (request.readyState !== 4 && !xDomain)) {
        return;
      }
      if (request.status === 0) {
        return;
      }
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: transformData(responseData, responseHeaders, config.transformResponse),
        status: request.status === 1223 ? 204 : request.status,
        statusText: request.status === 1223 ? 'No Content' : request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };
      settle(resolve, reject, response);
      request = null;
    };
    request.onerror = function handleError() {
      reject(new Error('Network Error'));
      request = null;
    };
    request.ontimeout = function handleTimeout() {
      var err = new Error('timeout of ' + config.timeout + 'ms exceeded');
      err.timeout = config.timeout;
      err.code = 'ECONNABORTED';
      reject(err);
      request = null;
    };
    if (utils.isStandardBrowserEnv()) {
      var cookies = require('../helpers/cookies');
      var xsrfValue = config.withCredentials || isURLSameOrigin(config.url) ? cookies.read(config.xsrfCookieName) : undefined;
      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          delete requestHeaders[key];
        } else {
          request.setRequestHeader(key, val);
        }
      });
    }
    if (config.withCredentials) {
      request.withCredentials = true;
    }
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        if (request.responseType !== 'json') {
          throw e;
        }
      }
    }
    if (config.progress) {
      if (config.method === 'post' || config.method === 'put') {
        request.upload.addEventListener('progress', config.progress);
      } else if (config.method === 'get') {
        request.addEventListener('progress', config.progress);
      }
    }
    if (requestData === undefined) {
      requestData = null;
    }
    request.send(requestData);
  };
})(require('process'));
