@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Fixtures <a href="{{ url('/fixtures/create') }}" class="btn btn-primary pull-right btn-sm">Add New Fixture</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th>{{ trans('fixtures.title') }}</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($fixtures as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('fixtures', $item->id) }}">{{ $item->title }}</a></td>
                    <td>
                        <a href="{{ url('/fixtures/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/fixtures', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $fixtures->render() !!} </div>
    </div>

</div>
@endsection
