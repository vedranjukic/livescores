@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Fixture</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ trans('fixtures.title') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $fixture->id }}</td> <td> {{ $fixture->title }} </td>
                </tr>
            </tbody>
        </table>
    </div>

</div>
@endsection