@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Score</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ trans('scores.fixture_id') }}</th><th>{{ trans('scores.score') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $score->id }}</td> <td> {{ $score->fixture_id }} </td><td> {{ $score->score }} </td>
                </tr>
            </tbody>
        </table>
    </div>

</div>
@endsection