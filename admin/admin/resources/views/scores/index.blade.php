@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Scores <a href="{{ url('/scores/create') }}" class="btn btn-primary pull-right btn-sm">Add New Score</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th>{{ trans('scores.fixture_id') }}</th><th>{{ trans('scores.score') }}</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($scores as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('scores', $item->id) }}">{{ $item->fixture_id }}</a></td><td>{{ $item->score }}</td>
                    <td>
                        <a href="{{ url('/scores/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/scores', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $scores->render() !!} </div>
    </div>

</div>
@endsection
