<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Fixture;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Redis;

class FixturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $fixtures = Fixture::paginate(15);

        return view('fixtures.index', compact('fixtures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('fixtures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        $fixture = Fixture::create($request->all());

        $obj = new \stdClass();
        $obj->event = 'fixture_add';
        $obj->data = $fixture;
        
        Redis::publish('admin', json_encode($obj));

        Session::flash('flash_message', 'Fixture added!');

        return redirect('fixtures');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $fixture = Fixture::findOrFail($id);

        return view('fixtures.show', compact('fixture'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $fixture = Fixture::findOrFail($id);

        return view('fixtures.edit', compact('fixture'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $fixture = Fixture::findOrFail($id);
        $fixture->update($request->all());

        $obj = new \stdClass();
        $obj->event = 'fixture_update';
        $obj->data = $fixture;
        
        Redis::publish('admin', json_encode($obj));

        Session::flash('flash_message', 'Fixture updated!');

        return redirect('fixtures');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Fixture::destroy($id);

        $obj = new \stdClass();
        $obj->event = 'fixture_delete';
        $obj->data = $id;
        
        Redis::publish('admin', json_encode($obj));

        Session::flash('flash_message', 'Fixture deleted!');

        return redirect('fixtures');
    }
}
