<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Score;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Redis;

class ScoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $scores = Score::paginate(15);

        return view('scores.index', compact('scores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('scores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        $score = Score::create($request->all());

        $obj = new \stdClass();
        $obj->event = 'score_change';
        $obj->data = $score;
        
        Redis::publish('admin', json_encode($obj));

        Session::flash('flash_message', 'Score added!');

        return redirect('scores');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $score = Score::findOrFail($id);

        return view('scores.show', compact('score'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $score = Score::findOrFail($id);

        return view('scores.edit', compact('score'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $score = Score::findOrFail($id);
        $score->update($request->all());

        $obj = new \stdClass();
        $obj->event = 'score_change';
        $obj->data = $score;
        
        Redis::publish('admin', json_encode($obj));

        Session::flash('flash_message', 'Score updated!');

        return redirect('scores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Score::destroy($id);

        Session::flash('flash_message', 'Score deleted!');

        return redirect('scores');
    }
}
